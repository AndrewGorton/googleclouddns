package uk.andrewgorton.googleclouddns;


import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.dns.Dns;
import com.google.api.services.dns.DnsScopes;
import com.google.api.services.dns.model.*;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.andrewgorton.googleclouddns.configuration.Config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MainModule {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainModule.class);

    public static void main(String[] args) {
        try {
            new MainModule().run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run(String[] args) throws Exception {
        LOGGER.info(String.format("Reading configuration from file '%s'", args[0]));
        ObjectMapper om = new ObjectMapper();
        Config config = om.readValue(new File(args[0]), Config.class);

        JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        LOGGER.info(String.format("Getting access token for '%s'", config.getEmailAddress()));
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(JSON_FACTORY)
                .setServiceAccountId(config.getEmailAddress())
                .setServiceAccountPrivateKeyFromP12File(new File(config.getP12file()))
                .setServiceAccountScopes(DnsScopes.all())
                .build();
        credential.refreshToken();
        String accessToken = credential.getAccessToken();

        Dns d = new Dns(httpTransport, JSON_FACTORY, credential);

        LOGGER.info(String.format("Searching for zone: %s", config.getProjectId()));
        boolean found = false;
        ManagedZonesListResponse mzlr = d.managedZones().list(config.getProjectId()).execute();
        for (ManagedZone singleZone : mzlr.getManagedZones()) {
            if (singleZone.getName().compareToIgnoreCase(config.getZoneId()) == 0) {
                found = true;
            }
        }

        if (found) {
            LOGGER.info("Zone exists - getting entries");
            List<ResourceRecordSet> deletions = new ArrayList<ResourceRecordSet>();
            ResourceRecordSetsListResponse rrslr = d.resourceRecordSets().list(config.getProjectId(), config.getZoneId()).execute();
            LOGGER.debug(rrslr.toPrettyString());
            for (ResourceRecordSet singleRrs : rrslr.getRrsets()) {
                // Leave out the SOA and NS, otherwise you can't delete the zone
                if (singleRrs.getType().compareToIgnoreCase("SOA") != 0 && singleRrs.getType().compareToIgnoreCase("NS") != 0) {
                    deletions.add(singleRrs);
                }
            }

            if (deletions.size() > 0) {
                LOGGER.info("Deleting existing zones");
                Change c = new Change();
                c.setDeletions(deletions);
                d.changes().create(config.getProjectId(), config.getZoneId(), c).execute();
            }

            //System.out.println("Deleting zone");
            //System.out.println(d.managedZones().delete(PROJECT_ID, ZONE_ID).execute());
        } else {
            LOGGER.info(String.format("Creating zone '%s' for domain '%s'", config.getZoneId(), config.getDnsDomain()));
            ManagedZone mz = new ManagedZone()
                    .setDnsName(config.getDnsDomain())
                    .setDescription(config.getDescription())
                    .setName(config.getZoneId());
            d.managedZones().create(config.getProjectId(), mz).execute();
        }

        LOGGER.info(String.format("Creating records from file '%s'", config.getZoneDataFile()));
        Change c = om.readValue(new File(config.getZoneDataFile()), Change.class);
        LOGGER.debug(d.changes().create(config.getProjectId(), config.getZoneId(), c).execute().toPrettyString());

        LOGGER.info(String.format("Getting nameservers for domain '%s'", config.getDnsDomain()));
        List<String> ns = d.managedZones().get(config.getProjectId(), config.getZoneId()).execute().getNameServers();
        for (String singleNs : ns) {
            LOGGER.info(singleNs);
        }
    }
}
