package uk.andrewgorton.googleclouddns.configuration;

public class Config {
    private String emailAddress;
    private String p12file;
    private String projectId;
    private String zoneId;
    private String zoneDataFile;
    private String dnsDomain;
    private String description;

    public Config() {

    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getP12file() {
        return p12file;
    }

    public void setP12file(String p12file) {
        this.p12file = p12file;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getZoneDataFile() {
        return zoneDataFile;
    }

    public void setZoneDataFile(String zoneDataFile) {
        this.zoneDataFile = zoneDataFile;
    }

    public String getDnsDomain() {
        return dnsDomain;
    }

    public void setDnsDomain(String dnsDomain) {
        this.dnsDomain = dnsDomain;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
