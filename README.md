# README.md for GoogleCloudDns

Sample showing how to programmatically use [Google Cloud DNS](https://cloud.google.com/dns/) for a domain.

You'll need a Google Project with the DNS API enabled, and an OAUTH2 Service Account for the project (the one which gives you an email address and a p12 file).

## Compile

Pre-requisites:-

* Java 1.7.0_45
* Maven 3.2.3

```bash
mvn clean package
```

## Configuration
Save the P12 file somewhere. Create a configuration file based on the below with your own details (or use the configuration/config.json.sample as a guide).

```json
{
   "emailAddress" : "example@developer.gserviceaccount.com",
   "p12file" : "oauth2.p12",
   "projectId" : "google-project-123",
   "zoneId" : "dns-examplecom",
   "zoneDataFile" : "example.com.json",
   "dnsDomain" : "example.com.",
   "description" : "DNS for example.com"
}
```

You'll also need a JSON-formatted file containing the DNS records you want (a sample file called 'example.com.json' can be used as a guide).

```json
{
   "additions":[
      {
         "kind":"dns#resourceRecordSet",
         "name":"example.com.",
         "rrdatas":[
            "93.184.216.119"
         ],
         "ttl":21600,
         "type":"A"
      },
      {
         "kind":"dns#resourceRecordSet",
         "name":"example.com.",
         "rrdatas":[
            "10 aspmx.l.google.com"
         ],
         "ttl":21600,
         "type":"MX"
      },
      {
         "kind":"dns#resourceRecordSet",
         "name":"www.example.com.",
         "rrdatas":[
            "example.com."
         ],
         "ttl":21600,
         "type":"CNAME"
      }
   ]
}
```


## Running

```bash
$ java -jar target/GoogleCloudDns-1.0.1-SNAPSHOT.jar <configurationFile>
```
